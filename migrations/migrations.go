package migrations

import (
	"github.com/gobuffalo/packr/v2"
)

// Box needs to generate and return packrd Box with migrations scripts.
func Box() *packr.Box {
	return packr.New("scripts", "scripts")
}
