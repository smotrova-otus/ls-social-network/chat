-- +migrate Up

create table chats
(
    id         varchar(36)             not null,
    created_at TIMESTAMP DEFAULT now() NOT NULL,
    updated_at TIMESTAMP DEFAULT now() NOT NULL,
    deleted_at TIMESTAMP,

    constraint chats_pk
        primary key (id)
);

create table members
(
    member_id varchar(36) not null,
    chat_id   varchar(36) not null,

    constraint chats_pk
        primary key (chat_id, member_id)
);

alter table members
    add constraint members_chat__fk_member_id
        foreign key (chat_id) references chats (`id`)
            on update cascade on delete cascade;

create table messages
(
    id         varchar(36)             not null,
    created_at TIMESTAMP DEFAULT now() NOT NULL,
    updated_at TIMESTAMP DEFAULT now() NOT NULL,
    deleted_at TIMESTAMP,

    chat_id    varchar(36)             not null,
    sender_id  varchar(36)             not null,
    text       text                    not null,
    constraint messages_pk
        primary key (id)
);

alter table messages
    add constraint messages_chat__fk_member_id
        foreign key (chat_id) references chats (`id`)
            on update cascade on delete cascade;

-- +migrate Down


DROP TABLE IF EXISTS members;
DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS chats;