//+build wireinject

package main

import (
	"github.com/google/wire"
	"github.com/sirupsen/logrus"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat"
	chatmysql "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/datasources/mysql"
	"gitlab.com/smotrova-otus/ls-social-network/chat/config"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/clock"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/database"
)

func NewContainer(
	cfg *config.Config,
	logger *logrus.Entry,
	masterConn *database.MasterConnection,
	slavesConn *database.SlaveConnection,
	chatShardedPool chatmysql.ShardedPool,
) (*Container, func(), error) {
	panic(wire.Build(
		chat.Set,

		uuid.NewGeneratorImpl,
		wire.Bind(new(uuid.Generator), new(*uuid.GeneratorImpl)),

		clock.NewRealClock,
		wire.Bind(new(clock.Clock), new(*clock.RealClock)),

		wire.Bind(new(database.QueryerExecer), new(*database.MasterConnection)),
		wire.Bind(new(database.Queryer), new(*database.SlaveConnection)),

		wire.Struct(new(Container), "*"),
	))
}
