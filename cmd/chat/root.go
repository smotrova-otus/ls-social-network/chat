package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/smotrova-otus/libs/config"
	chatmysql "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/datasources/mysql"
	"gitlab.com/smotrova-otus/ls-social-network/chat/config"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/database"
)

var (
	cfgFile         string
	masterDB        *sqlx.DB
	masterConn      *database.MasterConnection
	slavesConn      *database.SlaveConnection
	chatShardedPool chatmysql.ShardedPool
	logger          *logrus.Entry
	cfg             = new(config.Config)
)

var preRunHook = func(cmd *cobra.Command, _ []string) error {
	err := configloader.LoadConfig(cfgFile, cfg)
	if err != nil {
		return fmt.Errorf("config loading failed: %w", err)
	}

	logger = initLogger(*cfg)

	masterConn, masterDB, err = connectToMasterDatabase(cfg)
	if err != nil {
		return err
	}

	slavesConn, err = connectToSlavesDatabase(cfg, masterConn)
	if err != nil {
		return err
	}

	chatShardedPool, err = connectChatShardedPool(cfg)
	if err != nil {
		return err
	}

	return nil
}

var postRunHook = func(cmd *cobra.Command, _ []string) error {
	_ = masterConn.Close()
	return nil
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:                "chat",
	Short:              "A brief description of your application",
	Long:               "",
	PersistentPreRunE:  preRunHook,
	PersistentPostRunE: postRunHook,
	Run:                serve,
	SilenceUsage:       true,
}

func serve(_ *cobra.Command, _ []string) {
	container, cleanup, err := NewContainer(
		cfg,
		logger,
		masterConn,
		slavesConn,
		chatShardedPool,
	)
	if err != nil {
		logger.Fatal(err)
	}
	defer cleanup()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		httpAddr := cfg.Transport.Http.Address
		logger.Infof("server start at %s", httpAddr)
		errs <- http.ListenAndServe(httpAddr, container.Handler())
	}()

	logger.Errorf("start application failed: %v", <-errs)
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.chat.yaml)")
}
