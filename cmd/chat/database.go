package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	chatmysql "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/datasources/mysql"
	"gitlab.com/smotrova-otus/ls-social-network/chat/config"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/database"
)

func connectToDatabase(dsn string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(100)
	return db, nil
}

func connectToMasterDatabase(cfg *config.Config) (*database.MasterConnection, *sqlx.DB, error) {
	db, err := connectToDatabase(calculateDSN(cfg.Database.Master))
	if err != nil {
		return nil, nil, fmt.Errorf("connecting to database failed: %w", err)
	}
	return &database.MasterConnection{QueryerExecerCloser: db}, db, nil
}

func connectToSlavesDatabase(cfg *config.Config, conn *database.MasterConnection) (*database.SlaveConnection, error) {
	// If slave is not specified just use the master connection only.
	if cfg.Database.Slaves == nil || len(cfg.Database.Slaves) == 0 {
		logger.Info("slave configuration is not specified, master connection will be used")
		return &database.SlaveConnection{QueryerCloser: conn}, nil
	}

	var slaves database.Pool
	for _, slaveCfg := range cfg.Database.Slaves {
		conn, err := connectToDatabase(calculateDSN(slaveCfg))
		if err != nil {
			return nil, fmt.Errorf("connecting to database failed: %w", err)
		}

		slaves = append(slaves, conn)
	}

	return &database.SlaveConnection{QueryerCloser: slaves}, nil
}

func connectChatShardedPool(cfg *config.Config) (chatmysql.ShardedPool, error) {
	var pool chatmysql.ShardedPool
	for _, dbConfig := range cfg.Messages.Databases {
		conn, err := connectToDatabase(calculateDSN(dbConfig))
		if err != nil {
			return nil, fmt.Errorf("connecting to database failed: %w", err)
		}

		pool = append(pool, conn)
	}

	return pool, nil
}

func calculateDSN(config config.DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?parseTime=true",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.Name,
	)
}
