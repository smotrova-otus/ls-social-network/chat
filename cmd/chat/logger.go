package main

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/smotrova-otus/ls-social-network/chat/config"
)

func initLogger(cfg config.Config) *logrus.Entry {
	l := logrus.New()
	level, err := logrus.ParseLevel(cfg.Logger.Level)
	if err == nil {
		l.SetLevel(level)
	}
	l.SetReportCaller(cfg.Logger.Caller)
	return logrus.NewEntry(l)
}
