package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/olekukonko/tablewriter"
	sqlmigrate "github.com/rubenv/sql-migrate"
	"github.com/spf13/cobra"

	"gitlab.com/smotrova-otus/libs/migrate"

	"gitlab.com/smotrova-otus/ls-social-network/chat/migrations"
)

var (
	migrateTableName           = "migrations"
	limitCmdFlagForUpMigrate   = 0
	limitCmdFlagForDownMigrate = 1
)

// rootMigrateCmd.
var rootMigrateCmd = &cobra.Command{
	Use:          "migrate",
	Short:        "migrate",
	Long:         "",
	SilenceUsage: true,
}

var migrateUpCmd = &cobra.Command{
	Use:   "up",
	Short: "Migrates the database to the most recent version available",
	RunE: func(cmd *cobra.Command, args []string) error {
		for _, db := range append([]*sqlx.DB{masterDB}, chatShardedPool...) {
			err := db.DB.Ping()
			if err != nil {
				return err
			}
			migrater := migrate.NewMigrater(
				db.DB,
				migrateTableName,
				cfg.Database.Master.Dialect,
				&sqlmigrate.PackrMigrationSource{
					// Migrations are placed at the root of the project in the migrations folder.
					Box: migrations.Box(),
				})
			count, err := migrater.Up(limitCmdFlagForUpMigrate)
			if err != nil {
				return fmt.Errorf("migrate up failed: %w", err)
			}
			_, err = fmt.Fprintln(
				cmd.OutOrStdout(),
				fmt.Sprintf("applied %d migrations", count),
			)
			if err != nil {
				return fmt.Errorf("print applied info error: %w", err)
			}
		}
		return nil
	},
	SilenceUsage: true,
}

var migrateDownCmd = &cobra.Command{
	Use:   "down",
	Short: "Undo a database migration",
	RunE: func(cmd *cobra.Command, args []string) error {
		for _, db := range append([]*sqlx.DB{masterDB}, chatShardedPool...) {
			migrater := migrate.NewMigrater(
				db.DB,
				migrateTableName,
				cfg.Database.Master.Dialect,
				&sqlmigrate.PackrMigrationSource{
					// Migrations are placed at the root of the project in the migrations folder.
					Box: migrations.Box(),
				})
			count, err := migrater.Down(limitCmdFlagForDownMigrate)
			if err != nil {
				return fmt.Errorf("migrate down failed: %w", err)
			}

			_, err = fmt.Fprintln(
				cmd.OutOrStdout(),
				fmt.Sprintf("rollbacked %d migrations", count),
			)
			if err != nil {
				return fmt.Errorf("print rollbacked info error: %w", err)
			}
		}

		return nil
	},
	SilenceUsage: true,
}

var migrateStatus = &cobra.Command{
	Use:   "status",
	Short: "migrate status",
	Long:  "",
	RunE: func(cmd *cobra.Command, args []string) error {
		for _, db := range append([]*sqlx.DB{masterDB}, chatShardedPool...) {
			migrater := migrate.NewMigrater(
				db.DB,
				migrateTableName,
				cfg.Database.Master.Dialect,
				&sqlmigrate.PackrMigrationSource{
					// Migrations are placed at the root of the project in the migrations folder.
					Box: migrations.Box(),
				})
			rows, err := migrater.Status()
			if err != nil {
				return fmt.Errorf("migrate status failed: %w", err)
			}
			table := tablewriter.NewWriter(cmd.OutOrStdout())
			table.SetHeader([]string{"Migration", "Applied", "Rollback count", "Error"})
			table.SetColWidth(60)
			table.SetColMinWidth(1, 42)
			for _, r := range rows {
				if r.Migrated {
					table.Append([]string{
						r.ID,
						r.AppliedAt.String(),
						fmt.Sprintf("%d", rows[r.ID].RollbackCount),
						r.Error,
					})
				} else {
					table.Append([]string{
						r.ID,
						"no",
						"",
						r.Error,
					})
				}
			}
			table.Render()
		}

		return nil
	},
	SilenceUsage: true,
}

func init() {
	rootCmd.AddCommand(rootMigrateCmd)
	rootMigrateCmd.AddCommand(migrateUpCmd)
	rootMigrateCmd.AddCommand(migrateDownCmd)
	rootMigrateCmd.AddCommand(migrateStatus)

	migrateUpCmd.PersistentFlags().IntVar(&limitCmdFlagForUpMigrate, "limit", 0, "Limit the number of migrations, 0 = unlimited")
	migrateDownCmd.PersistentFlags().IntVar(&limitCmdFlagForDownMigrate, "limit", 1, "Limit the number of migrations, default is 1")
}
