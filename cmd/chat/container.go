package main

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	chathttp "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/transport/http"
	"net/http"
	"os"
)

type Container struct {
	ChatHTTPHandlerSet chathttp.Set
}

func (c Container) Handler() http.Handler {
	router := mux.NewRouter()

	router.Methods("POST").
		Path("/v0/chats/{chatID}/messages").
		Handler(c.ChatHTTPHandlerSet.PostMessageHandler)

	router.Methods("GET").
		Path("/v0/chats/{chatID}/messages").
		Handler(c.ChatHTTPHandlerSet.GetMessagesHandler)

	router.Methods("POST").
		Path("/v0/chats").
		Handler(c.ChatHTTPHandlerSet.CreateChatHandler)

	router.Methods("GET").
		Path("/v0/chats").
		Handler(c.ChatHTTPHandlerSet.GetChatsHandler)

	return c.handleCors(router)
}

func (c Container) handleCors(router http.Handler) http.Handler {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	return handlers.CORS(originsOk, headersOk, methodsOk)(router)
}
