package config

type DBConfig struct {
	Dialect  string `yaml:"dialect" validate:"required"`
	Host     string `yaml:"host" validate:"required"`
	Port     int    `yaml:"port" validate:"required"`
	User     string `yaml:"user" validate:"required"`
	Password string `yaml:"password" validate:"required"`
	Name     string `yaml:"name" validate:"required"`
}

type Config struct {
	Transport struct {
		Http struct {
			Address string `yaml:"address" validate:"required"`
		} `yaml:"http" validate:"required"`
	} `yaml:"transport" validate:"required"`
	Database struct {
		Master DBConfig   `yaml:"master" validate:"required"`
		Slaves []DBConfig `yaml:"slaves" validate:"required"`
	} `yaml:"database" validate:"required"`
	Logger struct {
		Level  string `yaml:"level"`
		Caller bool   `yaml:"caller"`
	} `yaml:"logger"`
	Messages struct {
		Databases []DBConfig `yaml:"databases" validate:"required"`
	} `yaml:"messages" validate:"required"`
}
