package endpoint

import "github.com/go-kit/kit/endpoint"

var _ endpoint.Failer = ErrorResponse{}

type ErrorResponse struct {
	Err error
}

func (e ErrorResponse) Failed() error {
	return e.Err
}
