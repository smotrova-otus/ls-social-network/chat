package endpoint

import (
	"context"
	"github.com/cenkalti/backoff/v4"
	"github.com/davecgh/go-spew/spew"
	"github.com/go-kit/kit/endpoint"
	"github.com/sirupsen/logrus"
	"gitlab.com/smotrova-otus/libs/log"
)

func LoggerMiddleware(logger *logrus.Entry) endpoint.Middleware {
	return func(e endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			ctx = log.ContextWithLogger(ctx, logger)
			return e(ctx, request)
		}
	}
}

func LoggingRequestResponseMiddleware(name string) endpoint.Middleware {
	return func(e endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			logger := log.Must(ctx)

			logger.WithFields(logrus.Fields{
				"request": spew.Sdump(request),
			}).Debugf("start endpoint handling: %s", name)

			defer func() {
				logger.WithFields(logrus.Fields{
					"request":  spew.Sdump(request),
					"response": spew.Sdump(response),
				}).Debugf("end endpoint handling: %s", name)
			}()

			return e(ctx, request)
		}
	}
}

func RetryMiddleware(retryBackoff backoff.BackOff) endpoint.Middleware {
	return func(e endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			logger := log.Must(ctx)
			retryBackoff = backoff.WithContext(retryBackoff, ctx)

			err = backoff.Retry(func() error {
				response, err = e(ctx, request)
				if err != nil {
					logger.WithError(err).Warning("error handle endpoint, retrying")
					return err
				}
				return nil
			}, retryBackoff)
			if err != nil {
				logger.WithError(err).Warning("error handle endpoint: %v", err)
			}

			return response, err
		}
	}
}
