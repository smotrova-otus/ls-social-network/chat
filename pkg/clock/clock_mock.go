package clock

import (
	mock "github.com/stretchr/testify/mock"
)
import time "time"

// ClockMock is an  mock type for the ClockMock type
type ClockMock struct {
	mock.Mock
}

// Now provides a mock function with given fields:
func (m *ClockMock) Now() time.Time {
	results := m.Called()
	return results.Get(0).(time.Time)
}
