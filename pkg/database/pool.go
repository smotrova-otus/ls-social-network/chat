package database

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"math/rand"
)

type Pool []*sqlx.DB

var _ QueryerExecerCloser = Pool{}

func (p Pool) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return p.conn().QueryContext(ctx, query, args)
}

func (p Pool) QueryxContext(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error) {
	return p.conn().QueryxContext(ctx, query, args)
}

func (p Pool) QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row {
	return p.conn().QueryRowxContext(ctx, query, args)
}

func (p Pool) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return p.conn().ExecContext(ctx, query, args)
}

func (p Pool) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return p.conn().Query(query, args...)
}

func (p Pool) Queryx(query string, args ...interface{}) (*sqlx.Rows, error) {
	return p.conn().Queryx(query, args...)
}

func (p Pool) QueryRowx(query string, args ...interface{}) *sqlx.Row {
	return p.conn().QueryRowx(query, args...)
}

func (p Pool) Exec(query string, args ...interface{}) (sql.Result, error) {
	return p.conn().Exec(query, args...)
}

func (p Pool) Close() error {
	for _, db := range p {
		err := db.Close()
		if err != nil {
			return err
		}
	}
	return nil
}

func (p Pool) conn() *sqlx.DB {
	return p[rand.Intn(len(p)-1)]
}
