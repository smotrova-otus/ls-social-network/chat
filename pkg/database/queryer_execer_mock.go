package database

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/mock"
	"reflect"
	"unsafe"
)

// QueryerExecerMock is an mock type for the type QueryerExecer
type QueryerExecerMock struct {
	mock.Mock
}

// NewQueryerExecerMock is a constructor.
func NewQueryerExecerMock() *QueryerExecerMock {
	return &QueryerExecerMock{}
}

func (m QueryerExecerMock) Exec(query string, args ...interface{}) (sql.Result, error) {
	results := m.Called(query, args)
	return results.Get(0).(sql.Result), results.Error(1)
}

func (m QueryerExecerMock) Query(query string, args ...interface{}) (*sql.Rows, error) {
	results := m.Called(query, args)
	return results.Get(0).(*sql.Rows), results.Error(1)
}

func (m QueryerExecerMock) Queryx(query string, args ...interface{}) (*sqlx.Rows, error) {
	results := m.Called(query, args)
	return results.Get(0).(*sqlx.Rows), results.Error(1)
}

func (m QueryerExecerMock) QueryRowx(query string, args ...interface{}) *sqlx.Row {
	results := m.Called(query, args)
	return results.Get(0).(*sqlx.Row)
}

// NewSqlxRowWithError uses for testing.
func NewSqlxRowWithError(err error) *sqlx.Row {
	var row = &sqlx.Row{
		Mapper: nil,
	}
	rRow := reflect.Indirect(reflect.ValueOf(row))
	rField := rRow.FieldByName("err")
	rVal := reflect.NewAt(rField.Type(), unsafe.Pointer(rField.UnsafeAddr()))
	reflect.Indirect(rVal).Set(reflect.ValueOf(err))
	return row
}
