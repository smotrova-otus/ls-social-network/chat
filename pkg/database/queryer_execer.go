package database

import (
	"github.com/jmoiron/sqlx"
	"io"
)

type QueryerExecer interface {
	sqlx.Queryer
	sqlx.QueryerContext
	sqlx.Execer
	sqlx.ExecerContext
}

type QueryerExecerCloser interface {
	QueryerExecer
	io.Closer
}

type Queryer interface {
	sqlx.Queryer
	sqlx.QueryerContext
}

type QueryerCloser interface {
	Queryer
	io.Closer
}
