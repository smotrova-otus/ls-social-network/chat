package application

import (
	"context"

	"gitlab.com/smotrova-otus/libs/log"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
)

type MessageRepository interface {
	PostMessage(
		context.Context,
		domain.Message,
	) error

	GetMessages(
		ctx context.Context,
		chatID uuid.UUID,
		offset, limit int,
	) ([]domain.Message, error)
}

type ChatRepository interface {
	CreateChat(
		ctx context.Context,
		chat domain.Chat,
	) error
	GetChatsByMember(
		ctx context.Context,
		memberID uuid.UUID,
		offset, limit int,
	) (chats []domain.Chat, err error)
}

type MemberRepository interface {
	GetMembersByIds(ids []uuid.UUID) (members map[uuid.UUID]domain.Member, error error)
}

type Service struct {
	messageRepository MessageRepository
	chatRepository    ChatRepository
	memberRepository  MemberRepository
}

func NewService(messageRepository MessageRepository,
	chatRepository ChatRepository,
	memberRepository MemberRepository,
) *Service {
	return &Service{
		messageRepository: messageRepository,
		chatRepository:    chatRepository,
		memberRepository:  memberRepository,
	}
}

func (s Service) PostMessage(ctx context.Context, msg domain.Message) (domain.Message, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "PostMessage")
	// TODO  check sender can write in chat
	err := s.messageRepository.PostMessage(ctx, msg)
	if err != nil {
		logger.
			WithError(err).
			Errorf("post message failed: save in repository failed (chat: %s)", msg.ChatID)
		return msg, err
	}
	logger.
		Infof("post message succeed (chat: %s)", msg.ChatID)
	return msg, err
}

func (s Service) GetMessages(
	ctx context.Context,
	chatID uuid.UUID,
	offset, limit int,
) ([]domain.Message, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "GetMessages")
	messages, err := s.messageRepository.GetMessages(ctx, chatID, offset, limit)
	if err != nil {
		logger.
			WithError(err).
			Errorf("get messages failed: get from repository failed (chat: %s)", chatID)
		return nil, err
	}
	if messages == nil {
		messages = make([]domain.Message, 0)
	}
	logger.
		Infof("get messages succeed (chat: %s)", chatID)
	return messages, nil
}

func (s Service) GetChats(
	ctx context.Context,
	memberID uuid.UUID,
	offset, limit int,
) ([]domain.Chat, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "GetChats")
	chats, err := s.chatRepository.GetChatsByMember(ctx, memberID, offset, limit)
	if err != nil {
		logger.
			WithError(err).
			Errorf("get chats failed: get from repository failed (member: %s)", memberID)
		return nil, err
	}
	if chats == nil {
		logger.
			Infof("get chats succeed (member: %s)", memberID)
		return make([]domain.Chat, 0), nil
	}

	memberIds := domain.Chats(chats).RecipientIDs(memberID)

	members, err := s.memberRepository.GetMembersByIds(memberIds)
	if err != nil {
		logger.
			WithError(err).
			Errorf("get chats failed: fill members failed(member: %s)", memberID)
		return nil, err
	}
	for key, c := range chats {
		for _, mID := range c.MemberIds {
			if mID == memberID {
				continue
			}
			chats[key].Members = []domain.Member{
				members[mID], // fill members without memberID
			}
		}
	}
	logger.
		Infof("get chats succeed (member: %s)", memberID)
	return chats, nil
}

func (s Service) CreateChat(ctx context.Context, chat domain.Chat) (domain.Chat, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "CreateChat")
	// TODO check chat is already exist
	err := s.chatRepository.CreateChat(ctx, chat)
	if err != nil {
		logger.
			WithError(err).
			Errorf("create chat failed: save in repository failed (members: %v)", chat.MemberIds)
	}
	logger.
		Infof("create chat succeed (members: %v)", chat.MemberIds)
	return chat, err
}
