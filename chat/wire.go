package chat

import (
	"github.com/google/wire"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/application"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/datasources"
	datasourcemysql "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/datasources/mysql"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/endpoints"
	transporthttp "gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/transport/http"
)

var Set = wire.NewSet(
	application.NewService,
	transporthttp.New,
	endpoints.New,
	wire.Bind(new(endpoints.Service), new(*application.Service)),
	datasourcemysql.NewMessageRepository,
	wire.Bind(new(application.MessageRepository), new(*datasourcemysql.MessageRepository)),
	datasourcemysql.NewChatRepository,
	wire.Bind(new(application.ChatRepository), new(*datasourcemysql.ChatRepository)),
	datasources.NewMemberRepository,
	wire.Bind(new(application.MemberRepository), new(*datasources.MemberRepository)),
)
