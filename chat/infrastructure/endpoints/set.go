package endpoints

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
)

type Set struct {
	PostMessageEndpoint endpoint.Endpoint
	GetMessagesEndpoint endpoint.Endpoint
	GetChatsEndpoint    endpoint.Endpoint
	CreateChatEndpoint  endpoint.Endpoint
}

type Service interface {
	PostMessage(ctx context.Context, msg domain.Message) (domain.Message, error)
	GetMessages(
		ctx context.Context,
		chatID uuid.UUID,
		offset, limit int,
	) ([]domain.Message, error)
	CreateChat(ctx context.Context, chat domain.Chat) (domain.Chat, error)
	GetChats(
		ctx context.Context,
		memberID uuid.UUID,
		offset, limit int,
	) ([]domain.Chat, error)
}

func New(svc Service) Set {
	return Set{
		PostMessageEndpoint: NewPostMessageEndpoint(svc),
		GetMessagesEndpoint: NewGetMessagesEndpoint(svc),
		GetChatsEndpoint:    NewGetChatsEndpoint(svc),
		CreateChatEndpoint:  NewCreateChatEndpoint(svc),
	}
}

func NewPostMessageEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(PostMessageRequest)
		msg, err := s.PostMessage(ctx, req.Message)
		return PostMessageResponse{Message: msg, Err: err}, nil
	}
}

func NewGetMessagesEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetMessagesRequest)
		msgs, err := s.GetMessages(ctx, req.ChatID, req.Offset, req.Limit)
		return GetMessagesResponse{Messages: msgs, Err: err}, nil
	}
}

func NewCreateChatEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(CreateChatRequest)
		chat, err := s.CreateChat(ctx, req.Chat)
		return CreateChatResponse{Chat: chat, Err: err}, nil
	}
}

func NewGetChatsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetChatsRequest)
		chats, err := s.GetChats(ctx, req.MemberID, req.Offset, req.Limit)
		return GetChatsResponse{Chats: chats, Err: err}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = GetMessagesResponse{}
	_ endpoint.Failer = PostMessageResponse{}
)

type PostMessageRequest struct {
	domain.Message
}

type PostMessageResponse struct {
	domain.Message
	Err error `json:"-"`
}

// Failed implements endpoint.Failer.
func (p PostMessageResponse) Failed() error {
	return p.Err
}

type GetMessagesRequest struct {
	ChatID uuid.UUID
	Limit  int
	Offset int
}

type GetMessagesResponse struct {
	Messages []domain.Message `json:"data"`
	Err      error            `json:"-"`
}

// Failed implements endpoint.Failer.
func (p GetMessagesResponse) Failed() error {
	return p.Err
}

type CreateChatRequest struct {
	domain.Chat
}

type CreateChatResponse struct {
	domain.Chat
	Err error `json:"-"`
}

// Failed implements endpoint.Failer.
func (p CreateChatResponse) Failed() error {
	return p.Err
}

type GetChatsRequest struct {
	MemberID uuid.UUID
	Limit    int
	Offset   int
}

type GetChatsResponse struct {
	Chats []domain.Chat `json:"data"`
	Err   error         `json:"-"`
}

// Failed implements endpoint.Failer.
func (p GetChatsResponse) Failed() error {
	return p.Err
}
