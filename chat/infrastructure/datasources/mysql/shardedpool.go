package mysql

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/database"
)

type ShardedPool []*sqlx.DB

func (p ShardedPool) Shard(hash []byte) database.QueryerExecer {
	return p[shardNum(hash, len(p))]
}

func shardNum(hashKey []byte, nodeCount int) int {
	sum := byte(0)
	for _, b := range hashKey {
		sum += b
	}
	return int(sum) % nodeCount
}
