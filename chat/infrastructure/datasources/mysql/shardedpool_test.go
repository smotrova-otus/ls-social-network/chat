package mysql

import (
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"testing"
)

func uuid2bytes(raw string) []byte {
	id := uuid.MustParse(raw)
	return id[:]
}

func Test_shardNum(t *testing.T) {
	type args struct {
		hash      []byte
		nodeCount int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "3 nodes, 0 hash sum, 0-index node selected",
			args: args{hash: []byte{0}, nodeCount: 3},
			want: 0,
		},
		{
			name: "3 nodes, 1 hash sum, 1-index node selected",
			args: args{hash: []byte{1}, nodeCount: 3},
			want: 1,
		},
		{
			name: "3 nodes, 2 hash sum, 2-index node selected",
			args: args{hash: []byte{2}, nodeCount: 3},
			want: 2,
		},
		{
			name: "3 nodes, 3 hash sum, 0-index node selected",
			args: args{hash: []byte{3}, nodeCount: 3},
			want: 0,
		},
		{
			name: "uuid hashing#1",
			args: args{hash: uuid2bytes("00000000-0000-0000-0000-000000000000"), nodeCount: 3},
			want: 0,
		},
		{
			name: "uuid hashing#2",
			args: args{hash: uuid2bytes("00000000-0000-0000-0000-000000000001"), nodeCount: 3},
			want: 1,
		},
		{
			name: "uuid hashing#3",
			args: args{hash: uuid2bytes("00000000-0000-0000-0000-000000000101"), nodeCount: 3},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shardNum(tt.args.hash, tt.args.nodeCount); got != tt.want {
				t.Errorf("shardNum() = %v, want %v", got, tt.want)
			}
		})
	}
}
