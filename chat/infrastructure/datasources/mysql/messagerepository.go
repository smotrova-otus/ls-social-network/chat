package mysql

import (
	"bytes"
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
	"sort"
)

type MessageRepository struct {
	pool ShardedPool
}

func NewMessageRepository(pool ShardedPool) *MessageRepository {
	return &MessageRepository{
		pool: pool,
	}
}

func (m MessageRepository) PostMessage(
	ctx context.Context,
	message domain.Message,
) error {
	hashKey := m.hashKey(
		message.ChatID,
	)

	_, err := m.pool.
		Shard(hashKey).
		ExecContext(
			ctx,
			"insert into messages"+
				" (id, created_at, updated_at, sender_id, chat_id, text)"+
				" values(?,?,?,?,?,?)",
			message.ID,
			message.CreatedAt,
			message.UpdatedAt,
			message.SenderID,
			message.ChatID,
			message.Text,
		)

	return err
}

func (m MessageRepository) GetMessages(
	ctx context.Context,
	chatID uuid.UUID,
	offset, limit int,
) (messages []domain.Message, err error) {
	hashKey := m.hashKey(
		chatID,
	)

	err = sqlx.SelectContext(
		ctx,
		m.pool.Shard(hashKey),
		&messages,
		"select * from messages where"+
			" chat_id=?"+
			" order by created_at desc"+
			" limit ? offset ?",
		chatID,
		limit, offset,
	)
	return messages, err
}

func (m MessageRepository) hashKey(uuids ...uuid.UUID) []byte {
	sort.Sort(uuid.UUIDs(uuids))
	var hash bytes.Buffer
	for _, u := range uuids {
		hash.Write(u[:])
	}
	return hash.Bytes()
}
