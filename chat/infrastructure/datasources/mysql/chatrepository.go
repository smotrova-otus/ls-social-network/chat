package mysql

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
	"gitlab.com/smotrova-otus/ls-social-network/chat/pkg/database"
)

type ChatRepository struct {
	writeConn database.QueryerExecer
	readConn  database.Queryer
}

func NewChatRepository(
	writeConn database.QueryerExecer,
	readConn database.Queryer,
) *ChatRepository {
	return &ChatRepository{
		writeConn: writeConn,
		readConn:  readConn,
	}
}

func (r ChatRepository) CreateChat(
	ctx context.Context,
	chat domain.Chat,
) error {
	// TODO do it in transaction
	_, err := r.writeConn.
		ExecContext(
			ctx,
			"insert into chats"+
				" (id, created_at, updated_at)"+
				" values(?,?,?)",
			chat.ID,
			chat.CreatedAt,
			chat.UpdatedAt,
		)
	if err != nil {
		return err
	}
	for _, m := range chat.MemberIds {
		_, err := r.writeConn.
			ExecContext(
				ctx,
				"insert into members"+
					" (member_id, chat_id)"+
					" values(?,?)",
				m,
				chat.ID,
			)
		if err != nil {
			return err
		}
	}
	return nil
}

type chatWithMember struct {
	ID        uuid.UUID  `db:"id"`
	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt time.Time  `db:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at"`
	MemberID1 uuid.UUID  `db:"member_id1"`
}

func (r ChatRepository) GetChatsByMember(
	ctx context.Context,
	memberID uuid.UUID,
	offset, limit int,
) (chats []domain.Chat, err error) {
	var rawChats []chatWithMember
	err = sqlx.SelectContext(
		ctx,
		r.readConn,
		&rawChats,
		"select c.*, m2.member_id as member_id1 from chats c "+
			"inner join members m on m.chat_id=c.id and m.member_id=? "+
			"left join members m2 on m2.chat_id=c.id and m2.member_id!=? "+
			"order by c.created_at desc "+
			"limit ? offset ? ",
		memberID,
		memberID,
		limit, offset,
	)
	for _, c := range rawChats {
		chats = append(chats, domain.Chat{
			ID:        c.ID,
			CreatedAt: c.CreatedAt,
			UpdatedAt: c.UpdatedAt,
			DeletedAt: c.DeletedAt,
			MemberIds: []uuid.UUID{
				memberID,
				c.MemberID1,
			},
		})
	}
	return chats, err
}
