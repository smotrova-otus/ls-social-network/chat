package datasources

import (
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
)

type Profile struct {
	ID        uuid.UUID
	FirstName string
	LastName  string
}
type ProfileRepository interface {
	FindByIDs(ids []uuid.UUID) (profiles map[uuid.UUID]Profile, error error)
}

type MemberRepository struct {
	profileClient ProfileRepository
}

func NewMemberRepository(profileClient ProfileRepository) *MemberRepository {
	return &MemberRepository{profileClient: profileClient}
}

func (r *MemberRepository) GetMembersByIds(ids []uuid.UUID) (members map[uuid.UUID]domain.Member, error error) {
	profiles, err := r.profileClient.FindByIDs(ids)
	if err != nil {
		return nil, err
	}
	members = make(map[uuid.UUID]domain.Member)
	for _, p := range profiles {
		members[p.ID] = domain.Member{
			ID:        p.ID,
			FirstName: p.FirstName,
			LastName:  p.LastName,
		}
	}
	return members, nil
}
