package http

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/go-kit/kit/endpoint"
	kitlogrus "github.com/go-kit/kit/log/logrus"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/sirupsen/logrus"
	pkghttp "gitlab.com/smotrova-otus/libs/http"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/domain"
	"gitlab.com/smotrova-otus/ls-social-network/chat/chat/infrastructure/endpoints"
	"net/http"
)

var (
	chatMuxVar = "chatID"
)

var ErrUnmarshalRequestBody = pkghttp.NewError(errors.New("unmarshal request body"), http.StatusBadRequest)
var ErrDecodeMemberID = pkghttp.NewError(errors.New("decode member id"), http.StatusBadRequest)
var ErrMemberIDIsRequired = pkghttp.NewError(errors.New("member id is required"), http.StatusBadRequest)
var ErrDecodeChatID = pkghttp.NewError(errors.New("decode chat id"), http.StatusBadRequest)
var ErrDecodeLimit = pkghttp.NewError(errors.New("decode limit"), http.StatusBadRequest)
var ErrDecodeOffset = pkghttp.NewError(errors.New("decode offset"), http.StatusBadRequest)

type Set struct {
	PostMessageHandler http.Handler
	GetMessagesHandler http.Handler
	CreateChatHandler  http.Handler
	GetChatsHandler    http.Handler
}

func New(endpoints endpoints.Set, logger *logrus.Entry) Set {
	return Set{
		PostMessageHandler: NewPostMessageHandler(endpoints.PostMessageEndpoint, logger),
		GetMessagesHandler: NewGetMessagesHandler(endpoints.GetMessagesEndpoint, logger),
		CreateChatHandler:  NewCreateChatHandler(endpoints.CreateChatEndpoint, logger),
		GetChatsHandler:    NewGetChatsHandler(endpoints.GetChatsEndpoint, logger),
	}
}

func NewPostMessageHandler(e endpoint.Endpoint, logger *logrus.Entry) http.Handler {
	kitlogger := kitlogrus.NewLogrusLogger(logger)

	return kithttp.NewServer(
		e,
		decodePostMessageRequest,
		pkghttp.EncodeJSONResponse,
		kithttp.ServerBefore(pkghttp.NewLoggerRequestMiddleware(logger)),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(kitlogger)),
	)
}

func NewGetMessagesHandler(e endpoint.Endpoint, logger *logrus.Entry) http.Handler {
	kitlogger := kitlogrus.NewLogrusLogger(logger)

	return kithttp.NewServer(
		e,
		decodeGetMessagesRequest,
		pkghttp.EncodeJSONResponse,
		kithttp.ServerBefore(pkghttp.NewLoggerRequestMiddleware(logger)),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(kitlogger)),
	)
}

func NewCreateChatHandler(e endpoint.Endpoint, logger *logrus.Entry) http.Handler {
	kitlogger := kitlogrus.NewLogrusLogger(logger)

	return kithttp.NewServer(
		e,
		decodeCreateChatRequest,
		pkghttp.EncodeJSONResponse,
		kithttp.ServerBefore(pkghttp.NewLoggerRequestMiddleware(logger)),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(kitlogger)),
	)
}

func NewGetChatsHandler(e endpoint.Endpoint, logger *logrus.Entry) http.Handler {
	kitlogger := kitlogrus.NewLogrusLogger(logger)

	return kithttp.NewServer(
		e,
		decodeGetChatsRequest,
		pkghttp.EncodeJSONResponse,
		kithttp.ServerBefore(pkghttp.NewLoggerRequestMiddleware(logger)),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(kitlogger)),
	)
}

func decodePostMessageRequest(_ context.Context, req *http.Request) (interface{}, error) {
	rawMsg := struct {
		Text     string    `json:"text"`
		SenderID uuid.UUID `json:"sender_id"`
	}{}

	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(&rawMsg)
	if err != nil {
		return nil, ErrUnmarshalRequestBody
	}

	chatID, err := pkghttp.DecodeUUIDRouteParam(chatMuxVar, req)
	if err != nil {
		return nil, ErrDecodeChatID
	}

	msg := domain.NewMessage(rawMsg.Text, rawMsg.SenderID, chatID)

	return endpoints.PostMessageRequest{Message: msg}, nil
}

func decodeGetMessagesRequest(_ context.Context, req *http.Request) (interface{}, error) {
	var err error
	var r endpoints.GetMessagesRequest

	r.ChatID, err = pkghttp.DecodeUUIDRouteParam(chatMuxVar, req)
	if err != nil {
		return nil, ErrDecodeChatID
	}

	var ok bool

	r.Limit, ok, err = pkghttp.DecodeIntQueryParam(req, "limit")
	if err != nil {
		return nil, ErrDecodeLimit
	}
	if !ok {
		r.Limit = 100
	}

	r.Offset, ok, err = pkghttp.DecodeIntQueryParam(req, "offset")
	if err != nil {
		return nil, ErrDecodeOffset
	}

	return r, nil
}

func decodeCreateChatRequest(_ context.Context, req *http.Request) (interface{}, error) {
	rawMsg := struct {
		MembersIds []uuid.UUID `json:"members_ids"`
	}{}

	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(&rawMsg)
	if err != nil {
		return nil, ErrUnmarshalRequestBody
	}

	chat, err := domain.NewChat(rawMsg.MembersIds)
	if err != nil {
		return nil, err
	}

	return endpoints.CreateChatRequest{Chat: chat}, nil
}

func decodeGetChatsRequest(_ context.Context, req *http.Request) (interface{}, error) {
	var err error
	var r endpoints.GetChatsRequest
	var ok bool
	r.MemberID, ok, err = pkghttp.DecodeUUIDQueryParam(req, "memberId")
	if err != nil {
		return nil, ErrDecodeMemberID
	}
	if !ok {
		return nil, ErrMemberIDIsRequired
	}

	r.Limit, ok, err = pkghttp.DecodeIntQueryParam(req, "limit")
	if err != nil {
		return nil, ErrDecodeLimit
	}
	if !ok {
		r.Limit = 100
	}

	r.Offset, ok, err = pkghttp.DecodeIntQueryParam(req, "offset")
	if err != nil {
		return nil, ErrDecodeOffset
	}

	return r, nil
}
