package domain

import (
	pkgerrors "gitlab.com/smotrova-otus/libs/errors"
	"net/http"
	"time"

	"gitlab.com/smotrova-otus/libs/uuid-go"
)

var (
	ErrMembersCount = &pkgerrors.Error{
		Err:    "chat must contain two members",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
)

type Chat struct {
	ID        uuid.UUID   `db:"id" json:"id"`
	CreatedAt time.Time   `db:"created_at" json:"created_at"`
	UpdatedAt time.Time   `db:"updated_at" json:"updated_at"`
	DeletedAt *time.Time  `db:"deleted_at" json:"deleted_at"`
	MemberIds []uuid.UUID `db:"-" json:"-"`
	Members   []Member    `json:"members"`
}

type Chats []Chat

func (chats Chats) RecipientIDs(senderID uuid.UUID) []uuid.UUID {
	var ids []uuid.UUID
	idsUnique := make(map[uuid.UUID]struct{})
	for _, c := range chats {
		for _, id := range c.MemberIds {
			if id == senderID {
				continue
			}
			_, ok := idsUnique[id]
			if ok {
				continue
			}
			idsUnique[id] = struct{}{}
			ids = append(ids, id)
		}
	}
	return ids
}

func NewChat(
	members []uuid.UUID,
) (Chat, error) {
	if len(members) != 2 {
		return Chat{}, ErrMembersCount
	}
	return Chat{
		ID:        uuid.Must(uuid.NewRandom()),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		MemberIds: members,
	}, nil
}
