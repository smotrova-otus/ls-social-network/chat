package domain

import "gitlab.com/smotrova-otus/libs/uuid-go"

type Member struct {
	ID        uuid.UUID `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
}
