package domain

import (
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"time"
)

type Message struct {
	ID        uuid.UUID  `db:"id" json:"id"`
	CreatedAt time.Time  `db:"created_at" json:"created_at"`
	UpdatedAt time.Time  `db:"updated_at" json:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at" json:"deleted_at"`
	SenderID  uuid.UUID  `db:"sender_id" json:"sender_id"`
	ChatID    uuid.UUID  `db:"chat_id" json:"chat_id"`
	Text      string     `db:"text" json:"text"`
}

func NewMessage(
	text string,
	senderID uuid.UUID,
	chatID uuid.UUID,
) Message {
	return Message{
		ID:        uuid.Must(uuid.NewRandom()),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Text:      text,
		SenderID:  senderID,
		ChatID:    chatID,
	}
}
