Run database
```bash
docker-compose up
```

Run migrations
```bash
./chat migrate up --config config.yml
```

Migrate down
```bash
./chat migrate down --limit=1 --config config.yml
```

Status of migrations
```bash
./chat migrate status --config config.yml
```

Serve
```bash
./chat --config config.yml
```

