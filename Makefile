GOPATH ?= $(HOME)/go
BIN_DIR = $(GOPATH)/bin
PACKR = $(BIN_DIR)/packr2
CMD_DIR = ./cmd/chat
BRANCH_NAME = $(shell git rev-parse --abbrev-ref HEAD)

$(SQLMIGRATE):
	go get github.com/rubenv/sql-migrate/sql-migrate

.PHONY: packr
$(PACKR):
	go get github.com/gobuffalo/packr/v2/packr2

.PHONY: tools
tools: $(SQLMIGRATE) $(PACKR)

.PHONY: migratebin
migratebin: $(PACKR)
	cd migrations; packr2
	$(MAKE) build

.PHONY: build
build: ## Build the project binary
	go build $(CMD_DIR)

.PHONY: migrateup
migrateup:
	go run $(CMD_DIR) migrate up --config config.yml

.PHONY: migratedown
migratedown:
	go run $(CMD_DIR) migrate down --limit=1 --config config.yml

.PHONY: migratestatus
migratestatus:
	go run $(CMD_DIR) migrate status --config config.yml

seed: build
	go run $(CMD_DIR) seed profiles --config config.yml

serve:
	go run $(CMD_DIR) --config config.yml

.PHONY: test
test:
	DSN="root:password@tcp(127.0.0.1:3307)/chat?parseTime=true" go test -short ./...

.PHONY: frontend/build
frontend/build:
	cd frontend; PUBLIC_URL=/ui yarn build

.PHONY: frontend/packrd
frontend/packrd:
	cd frontend; packr2

IMAGE_NAME := smotrova-otus/ls-social-network/chat
DOCKER_REGISTRY := registry.gitlab.com

.PHONY: docker-build
docker-build:
	docker build -t ${DOCKER_REGISTRY}/${IMAGE_NAME}:${BRANCH_NAME} .

.PHONY: docker-push
docker-push:
	docker push ${DOCKER_REGISTRY}/${IMAGE_NAME}:${BRANCH_NAME}

